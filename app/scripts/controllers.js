'use strict';

/**
 * @ngdoc function
 * @name twitcherYoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the twitcherYoApp
 */
angular.module('twitcherYoApp')

.filter('joinBy', function () {
    return function (input,delimiter) {
        return (input || []).join(delimiter || ',');
    };
})

.controller('ListCtrl', ['$scope', '$uibModal', function($scope, $uibModal) {
    
    $scope.filteredLists = [];
    $scope.currentPage = 1;
    $scope.numPerPage = 10;
    
    var lists =[
                 {
                   location: 'California',
                   timeframe: 'Year',
                   count: '1354'
                },
                {
                   location: 'California',
                   timeframe: 'Week',
                   count: '92'
                },
                {
                   location: 'Yard',
                   timeframe: 'Month',
                   count: '150'
                },
                {
                   location: 'San Francisco',
                   timeframe: 'Day',
                   count: '48'
                }
                ];

    $scope.lists = lists;
    
    $scope.$watch('currentPage + numPerPage', function() {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage);
        var end = begin + $scope.numPerPage;

        $scope.filteredLists = $scope.lists.slice(begin, end);
    });

    $scope.open = function (size) {
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'addList.html',
            controller: 'AddListCtrl',
            size: size,
            resolve: {
            }
      });
      
      modalInstance.result.then(function () {

      }, function () {

      });
  };
      
}])

.controller('AddListCtrl', ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {

    $scope.location = '';
    $scope.timeframe = '';
    $scope.from = '';
    $scope.to = '';
    
    $scope.$watch("timeframe", function(val){
        if (val != "custom") { 
            $scope.from = "";
            $scope.to = "";
        }
    });
    
    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    
}])

.controller('AchievementCtrl', ['$scope', function($scope) {

  $scope.myInterval = 5000;
  $scope.noWrapSlides = false;
  $scope.active = 0;
  var currIndex = 0;

    var achievements =[
                 {
                   id: 0,
                   name: '100 Species Observed',
                   desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ullamcorper maximus molestie. Phasellus accumsan ornare nulla vel vehicula.',
                   date: '2016-09-30',
                   icon: 'binoculars'
                },
                {
                   id: 1,
                   name: 'Raptor City',
                   desc: 'Vestibulum consectetur lacinia leo, ut fermentum velit eleifend quis. Integer imperdiet feugiat convallis.',
                   date: '2015-05-30',
                   icon: 'thumbs-up'
                },
                {
                   id: 2,
                   name: 'Knock knock',
                   desc: 'Morbi ac diam ullamcorper, convallis urna quis, sagittis dolor. Quisque malesuada odio non tortor lacinia vestibulum. Phasellus efficitur purus vitae odio gravida, at cursus dui iaculis.',
                   date: '2014-12-15',
                   icon: 'themeisle'
                },
                {
                   id: 3,
                   name: 'Chickadee-dee-dee',
                   desc: 'Etiam sagittis pretium justo non varius. Mauris ac consequat nisl, ac lacinia turpis. Cras iaculis, est eget euismod posuere, neque nibh lacinia enim, eget sagittis ligula sem cursus ligula.',
                   date: '2016-09-03',
                   icon: 'pied-piper-alt'
                }
                ];
    
    $scope.achievements = achievements;
    
    var achievementsInProgress =[
                 {
                   name: '100 Species Observed',
                   desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ullamcorper maximus molestie. Phasellus accumsan ornare nulla vel vehicula.',
                   progress: '90',
                   icon: 'binoculars'
                },
                {
                   name: 'Raptor City',
                   desc: 'Vestibulum consectetur lacinia leo, ut fermentum velit eleifend quis. Integer imperdiet feugiat convallis.',
                   progress: '56',
                   icon: 'thumbs-up'
                },
                {
                   name: 'Knock knock',
                   desc: 'Morbi ac diam ullamcorper, convallis urna quis, sagittis dolor. Quisque malesuada odio non tortor lacinia vestibulum. Phasellus efficitur purus vitae odio gravida, at cursus dui iaculis.',
                   progress: '34',
                   icon: 'themeisle'
                },
                {
                   name: 'Chickadee-dee-dee',
                   desc: 'Etiam sagittis pretium justo non varius. Mauris ac consequat nisl, ac lacinia turpis. Cras iaculis, est eget euismod posuere, neque nibh lacinia enim, eget sagittis ligula sem cursus ligula.',
                   progress: '16',
                   icon: 'pied-piper-alt'
                }
                ];
    
    $scope.achievementsInProgress = achievementsInProgress;
    
}])

.controller('NeedCtrl', ['$scope', function($scope) {

    $scope.filteredNeeds = [];
    $scope.currentPage = 1;
    $scope.numPerPage = 10;
    
    var needs =[
                 {
                   species: 'Yellow-rumped Warbler',
                   location: 'Madison',
                   lists: ['Year', 'Month'],
                   observers: ['Joh Doe', 'Jane Doe'],
                   lastObserved: '2016-09-29',
                   notes: ['John Doe: Very pretty.']
                },
                {
                   species: 'Yellow-rumped Warbler',
                   location: 'Nashville-Davidson',
                   lists: ['Year', 'Month'],
                   observers: ['Joh Doe', 'Jane Doe'],
                   lastObserved: '2016-09-29',
                   notes: ['John Doe: Very pretty.']
                },
                {
                   species: 'Yellow-rumped Warbler',
                   location: 'Columbus',
                   lists: ['Year', 'Month'],
                   observers: ['Joh Doe', 'Jane Doe'],
                   lastObserved: '2016-09-29',
                   notes: ['John Doe: Very pretty.']
                },
                {
                   species: 'Yellow-rumped Warbler',
                   location: 'Greensboro',
                   lists: ['Year', 'Month'],
                   observers: ['Joh Doe', 'Jane Doe'],
                   lastObserved: '2016-09-29',
                   notes: ['John Doe: Very pretty.']
                }
                ];

    $scope.needs = needs;
    
    $scope.$watch('currentPage + numPerPage', function() {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage);
        var end = begin + $scope.numPerPage;

        $scope.filteredNeeds = $scope.needs.slice(begin, end);
    });
    
}])

.controller('ObservationCtrl', ['$scope', '$uibModal', function($scope, $uibModal) {

    $scope.filteredObservations = [];
    $scope.currentPage = 1;
    $scope.numPerPage = 10;
    
    var observations =[
                 {
                   species: 'Yellow-rumped Warbler',
                   location: 'Minneapolis',
                   datetime: '2016-09-29 09:00',
                   count: 20,
                   notes: 'Very pretty.'
                },
                {
                   species: 'Magnolia Warbler',
                   location: 'New Orleans',
                   datetime: '2016-09-15 13:00',
                   count: 2,
                   notes: 'Very pretty.'
                },
                {
                   species: 'Black-capped Chickadee',
                   location: 'Plano',
                   datetime: '2014-01-12 19:00',
                   count: 30,
                   notes: 'Very pretty.'
                },
                {
                   species: 'Boreal Chickadee',
                   location: 'Scottsdale',
                   datetime: '2015-05-01 16:00',
                   count: 5,
                   notes: 'Very pretty.'
                }
                ];

    $scope.observations = observations;
    
    $scope.$watch('currentPage + numPerPage', function() {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage);
        var end = begin + $scope.numPerPage;

        $scope.filteredObservations = $scope.observations.slice(begin, end);
    });
    
    $scope.open = function (size) {
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'addObservation.html',
            controller: 'AddObservationCtrl',
            size: size,
            resolve: {
            }
      });
      
      modalInstance.result.then(function () {

      }, function () {

      });
    };
    
}])

.controller('AddObservationCtrl', ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {

    $scope.species = '';
    $scope.location = '';
    $scope.datetime = '';
    $scope.notes = '';
    $scope.media = '';
    
    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    
}])

.controller('BirdIdentificationCtrl', ['$scope', function($scope) {
    
    var speciesList =[
             {
               name: 'Prothonotary Warbler',
               desc: 'The prothonotary warbler is 13 cm (5.1 in) long and weighs 12.5 g (0.44 oz). It has an olive back with blue-grey wings and tail, yellow underparts, a relatively long pointed bill and black legs. The adult male has a bright orange-yellow head. Females and immature birds are duller and have a yellow head. In flight from below, the short, wide tail has a distinctive two-toned pattern, white at the base and dark at the tip.',
               image: 'images/prothonotary warbler.JPG'
            },
            {
               name: 'Veery',
               desc: 'This species measures 16–19.5 cm (6.3–7.7 in) in length. Its mass is 26–39 g (0.92–1.38 oz), exceptionally up to 54 g (1.9 oz). The wingspan averages 28.5 cm (11.2 in). Each wing measures 8.9–10.4 cm (3.5–4.1 in), the bill measures 1.2–1.9 cm (0.47–0.75 in) and the tarsus is 2.7–3.25 cm (1.06–1.28 in). The veery shows the characteristic underwing stripe of Catharus thrushes. Adults are mainly light brown on the upperparts. The underparts are white; the breast is light tawny with faint brownish spots. Veeries have pink legs and a poorly defined eye ring.',
               image: 'images/veery.JPG'
            },
            {
               name: 'Bay-breasted Warbler',
               desc: 'This species is closely related to blackpoll warbler, but this species has a more southerly breeding range and a more northerly wintering area. The summer male bay-breasted warblers are unmistakable. They have grey backs, black faces, and chestnut crowns, flanks and throats. They also boast of bright yellow neck patches, and their underparts are white. They have two white wing bars, as well.',
               image: 'images/bay-breasted warbler.JPG'
            },
            {
               name: 'Brown Creeper',
               desc: 'Adults are brown on the upper parts with light spotting, resembling a piece of tree bark, with white underparts. They have a long thin bill with a slight downward curve and a long stiff tail used for support as the bird creeps upwards. The male creeper has a slightly larger bill than the female. The brown creeper is 11.7–13.5 cm (4.6–5.3 in) long. Its voice includes single very high pitched, short, often insistent, piercing calls; see, or swee. The song often has a cadence like; pee pee willow wee or see tidle swee, with notes similar to the calls.',
               image: 'images/brown creeper.JPG'
            }
            ];

    $scope.speciesList = speciesList;
    
}])

;
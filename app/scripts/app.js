'use strict';

/**
 * @ngdoc overview
 * @name twitcherYoApp
 * @description
 * # twitcherYoApp
 *
 * Main module of the application.
 */
angular
  .module('twitcherYoApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/lists', {
        templateUrl: 'views/lists.html',
        controller: 'ListCtrl'
      })
      .when('/achievements', {
        templateUrl: 'views/achievements.html',
        controller: 'AchievementCtrl'
      })
      .when('/needs', {
        templateUrl: 'views/needs.html',
        controller: 'NeedCtrl'
      })
      .when('/observations', {
        templateUrl: 'views/observations.html',
        controller: 'ObservationCtrl'
      })
      .when('/bird-identification', {
        templateUrl: 'views/bird-identification.html',
        controller: 'BirdIdentificationCtrl'
      })
      .otherwise({
        redirectTo: '/lists'
      });
  });
